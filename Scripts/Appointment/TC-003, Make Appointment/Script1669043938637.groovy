import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.callTestCase(findTestCase('Login/TC-002, Login Success'), [('v_username') : findTestData('TD-Login').getValue(
            'v_username', 2), ('v_password') : findTestData('TD-Login').getValue('v_password', 2)], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForPageLoad(GlobalVariable.default_timeout)

int i_facility

switch (v_facility) {
    case 'Tokyo':
        i_facility = 0

        break
    case 'Hongkong':
        i_facility = 1

        break
    case 'Seoul':
        i_facility = 2

        break
    default:
        print("ERROR, Cannot select $v_facility")

        FailureHandling.STOP_ON_FAILURE}

WebUI.selectOptionByIndex(findTestObject('Object Repository/Page_Appointment/slc_Facility'), i_facility, FailureHandling.STOP_ON_FAILURE)

if (v_is_readmission.equals('True')) {
    WebUI.click(findTestObject('Object Repository/Page_Appointment/chk_Hospital Readmission'))
}

WebUI.click(findTestObject('Page_Appointment/rdb_Program', [('text') : v_program]))

WebUI.setText(findTestObject('Page_Appointment/txt_Visit Date'), v_visit_date)

WebUI.setText(findTestObject('Page_Appointment/txa_Comment'), v_comment)

WebUI.click(findTestObject('Object Repository/Page_Appointment/btn_Book Appointment'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Appointment/h2_Appointment Confirmation'), GlobalVariable.default_timeout)

