<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Menu Toggle</name>
   <tag></tag>
   <elementGuidId>9afd15c4-fca3-4744-bac8-079825aa41d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;menu-toggle&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@id = 'menu-toggle']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>9cb294e5-114d-4c44-b9b4-b63ea3a2c450</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>menu-toggle</value>
      <webElementGuid>1e77f059-a454-4198-9cca-a77b8b53e3be</webElementGuid>
   </webElementProperties>
</WebElementEntity>
