<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Logout</name>
   <tag></tag>
   <elementGuidId>5bf7f465-8568-4d0a-83be-8a7135cb1e97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@href=&quot;authenticate.php?logout&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = 'authenticate.php?logout']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7480a562-5b95-46d5-992b-f0c042ee5df4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>authenticate.php?logout</value>
      <webElementGuid>1c25959c-d3c9-42b1-8a3c-0cf255cfa57d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
